# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This program is used to develop and test the tools understanding of the JVM internal data structures. This program is run in a 32-bit VM environment, and then the machine is paused and the memory is captured.  After the memory from the memory is captured, the process memory is extracted using the Volatility Framework.  The process memory is then pre-processed with separate tools, which extract pointers and strings and import them into redis.

### How do I get set up? ###

* Install Eclipse
* Install Java 8
* Import this project into Eclipse

### Who do I talk to? ###

* Adam Pridgen <adam.pridgen@thecoverofnight.com>
