import java.util.ArrayList;
import java.util.List;



public class ExecuteStacks {
	Thread myThreadExecutor = null;
	String myName = null;
	String myTag = null;
	Integer myId = 0;
	
	ArrayList<ChildThreadSleeper> myChildrenThreads = null;
	public ExecuteStacks(String myname, Integer countToZero) {
		// TODO Auto-generated constructor stub
		myName = myname + "theid: " + countToZero.toString();
		myId = countToZero;
		myChildrenThreads = new ArrayList<ChildThreadSleeper>();
	}
	
	
	public void wakeup_all() {
		for (ChildThreadSleeper cs : myChildrenThreads) {
			cs.wakeup();
		}
	}
	public void startAllChildrenThreadsWithSleepFunc (int num) {
		while (num > 0) {
			ChildThreadSleeper cs = new ChildThreadSleeper(myName+" "+myId+":"+num, num);
			myChildrenThreads.add(cs);
			num--;
		}
		
		for (ChildThreadSleeper cs : myChildrenThreads) {
			cs.start();
		}
	}
		
	public static void main(String[] args) {
		ExecuteStacks x = new ExecuteStacks("Hey there buddy", 100);
		x.startAllChildrenThreadsWithSleepFunc(10);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		x.wakeup_all();
	}
}
