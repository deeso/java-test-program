import java.util.Random;


public class GetSomeField2ModifiedFieldTypeBytes {
	
	private String current_value = new String();
	private Integer number_of_changes = 1;
	private Long number_of_iterations = 10L;
	private Long last_number_of_iterations = 10L;
	private byte [] password = new byte[10];
	
	public GetSomeField2ModifiedFieldTypeBytes() {
		for (int i = 0; i < password.length; i++)
			password[i] = 0;
	}
	
	private void incr_number(){
		number_of_changes ++;
	}
	
	private void make_change(String c){
		number_of_changes ++;
		System.out.println("Current value: "+current_value);
		current_value = null;
		current_value = c;
		System.out.println("New value: "+current_value);
	}
	
	public void perform_a_task (){
		Random r = new Random();
		int magic_value = 559;
		
		String base_pass = new String("password");
		long k = 10000000000000000L; 
		while (k > 0){
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < 20; i++) {
				sb.append(String.format("%02x", (r.nextInt()& 0x0ff)));
			}
			
			String new_pas = base_pass + sb.toString();
			System.out.println("My Secret is:"+new_pas);
			// create alot of objects
			for (long t = 1000L; t > 0; t--) {
				number_of_iterations++;
				
				String p = " "+new_pas;
				Integer i = Integer.valueOf((int)t);
				if (r.nextInt() > 10) {
					System.err.println(i+p);
					r.nextBytes(this.password);
					break;
				}
	
			}
			k--;
			number_of_iterations++;
			if (number_of_iterations > 100000) {
				System.out.println("number of iterations are: "+number_of_iterations);
				System.out.println("last number of iterations are: "+last_number_of_iterations );
				last_number_of_iterations = number_of_iterations;
				number_of_iterations = (long) 0;
			}
				
		}
	}
	public static void main(String[] args) {
		GetSomeField2ModifiedFieldTypeBytes gf = new GetSomeField2ModifiedFieldTypeBytes();
		gf.perform_a_task();
	}
	
}
