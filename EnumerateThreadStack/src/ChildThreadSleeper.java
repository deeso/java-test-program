import java.util.Random;


public class ChildThreadSleeper extends Thread {
	Boolean needToSleep = true;
	String myName = null;
	Integer id = 0;
	Boolean debug = false;
	int num_calls = 10;
	Boolean doRun2 = true;
	public ChildThreadSleeper(String name, int id) {
		this.id = id;
		myName = name;
	}
	public ChildThreadSleeper(String name, int id, boolean run2) {
		this.id = id;
		myName = name;
		doRun2 = run2;
	}
	public ChildThreadSleeper(String name, int id, Boolean debug) {
		this.id = id;
		myName = name;
		this.debug = debug;
	}
	
	public void run () {
		if (doRun2)
			run2();
		else run1();
	}
	
	private void run1() {
		Random r = new Random();
		int magic_value = 559;
		String base_pass = new String("password");
		long k = num_calls*10000; 
		while (k > 0){
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < 20; i++) {
				sb.append(String.format("%02x", (r.nextInt()& 0x0ff)));
			}
			
			String new_pas = base_pass + sb.toString();
			System.out.println("My Secret is:"+new_pas);
			// create alot of objects
			for (long t = 10000000L; t > 0; t--) {
				String p = " "+new_pas;
				Integer i = Integer.valueOf((int)t);
				if (magic_value == t)
					System.err.println(i+p);

			}
			k--;
		}
			

		
	}
	public void run2 () {
		if (debug) System.out.println("Starting child thread."+myName);
		while (needToSleep) {
			doitalot(myName+"_"+id+"::", num_calls);
			if (debug && needToSleep) System.out.println("Starting child thread."+myName);
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void doitalot(String mut, int num) {
		Random r = new Random();
		String rc = Character.toString((char) (r.nextInt(26)+'a'));
		System.out.println("Calling doitalot:"+mut);
		doitalot (mut+"AOP::"+rc, num-- );
	}
	
	public void wakeup () {
		needToSleep = false;
		this.notify();
	}
}
